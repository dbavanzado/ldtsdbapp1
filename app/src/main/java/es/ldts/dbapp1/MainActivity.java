package es.ldts.dbapp1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

	private static Context context;

	private TextView pizarra;
	private Button refresh;

	private android.support.v7.widget.ShareActionProvider mShareActionProvider = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = getBaseContext();
		//El Contexto es el entorno de la aplicación: Carpetas, idioma, etc.
		setContentView(R.layout.activity_main);
		filemanager.setbook(getCacheDir().getAbsolutePath(), context);

		pizarra = (TextView) findViewById(R.id.pizarra);
		refresh = (Button) findViewById(R.id.refresh);

		//ActionBar es la barra superior de la aplicación
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		//Configuramos icono, título y menu
		getSupportActionBar().setIcon(R.mipmap.ic_launcher);
		setTitle(context.getResources().getString(R.string.app_name));
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		wrpizzara();

	}

	public void wrpizzara() {
		if (!db.isOpen()) db.open();
		String[][] aux = db.select("Select * from usuarios order by random()");

		String texto = "Listado De Usuarios\n";
		if (aux != null)
			for (int i = 0; i < aux.length; i++) {
				if (aux[i] != null)
					for (int j = 0; j < aux[i].length; j++)
						texto += aux[i][j] + " ";
				texto += "\n";
			}

		pizarra.setText(texto);
	}

	//Este método se crea para que el botón de Refresh pueda ejecutar wrpizarra.
	// Los botones siempre ejecutan métodos que recibien como parámetro el View actual.
	public void wrpizzara(View v) {
		wrpizzara();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.share_menu, menu);
		MenuItem itemShare = menu.findItem(R.id.menu_item_share);
		itemShare.setIcon(R.mipmap.share);
		// Fetch and store ShareActionProvider
		mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(itemShare);
		Intent j = new Intent(Intent.ACTION_SEND);
		setShareIntent(j);
		doShare();
		// Return true to display menu
		return true;
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		return true;
	}


	// SHARE BUTTON!!!!
	// Call to update the share intent
	//@Override
	private void setShareIntent(Intent shareIntent) {
		if (mShareActionProvider != null) {
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}

	public void doShare() {
		// populate the share intent with data
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, context.getResources().getString(R.string.sharetxt) +
				"\n" + context.getResources().getString(R.string.app_name) + "\n" +
				context.getResources().getString(R.string.sharelink)
		);
		mShareActionProvider.setShareIntent(intent);
	}

	private void exit() {
		new AlertDialog.Builder(this)
				.setTitle(R.string.app_name)
				.setMessage(context.getResources().getStringArray(R.array.exitbuttom)[0])
				.setPositiveButton(context.getResources().getStringArray(R.array.exitbuttom)[1], new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						//finish();
						//finishActivity(0);
						System.exit(0);
					}
				})
				.setNegativeButton(context.getResources().getStringArray(R.array.exitbuttom)[2], new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				})
				.show();
	}

	@Override
	public void setTitle(CharSequence title) {
		getSupportActionBar().setTitle(title);
	}

	@Override
	public void onBackPressed() {
		exit();
		//your code when back button pressed
	}


}
